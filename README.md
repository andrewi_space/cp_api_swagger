
## Creating a Swagger server 

1. If using OpenAPI 3.0 on the project folder, execute the following command:
$ npx swagger-node-codegen swagger.oas3.yaml -o ./server
$ npx swagger-node-codegen swagger.2.yaml -o ./server

2. Install dependencies for the server:ls

$ cd server
$ npm install

3
<uri>: file:///C:/Users/AndrewIgnatov/Projects/Bitbucket/sample_app/CP_API_swagger/cp_api_swagger/swagger.2.yaml

$ docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli generate -i file:///C:/Users/AndrewIgnatov/Projects/Bitbucket/sample_app/CP_API_swagger/cp_api_swagger/swagger.2.yaml -l nodejs-server -o /local/server
